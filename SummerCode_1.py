#reading from a file:
mylines = []                                # Declare an empty list.
with open ('nmea.txt', 'rt') as myfile:    # Open lorem.txt for reading text.
    for myline in myfile:                   # For each line in the file,
        mylines.append(myline.rstrip('\n')) # strip newline and add to list.
for element in mylines:                     # For each element in the list,
    print(element)                          # print it.
Data = mylines[0]




'''
FileName = open("nmea.txt","r+")
print("file closed?", FileName.closed)

GPSfile = FileName.read()
print(str(GPSfile) +"\n")

GPSstuff = []

print(GPSstuff)
'''


#FileName.close()